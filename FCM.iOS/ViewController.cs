﻿using Firebase.InstanceID;
using System;

using UIKit;

namespace FCM.iOS
{
    public partial class ViewController : UIViewController
    {
        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            InstanceId.Notifications.ObserveTokenRefresh((sender, e) => {
                var refreshedToken = InstanceId.SharedInstance.Token;
                Console.WriteLine(refreshedToken);
            });

            Console.WriteLine(InstanceId.SharedInstance.Token);
        }


    }
}