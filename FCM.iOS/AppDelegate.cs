﻿using Firebase.CloudMessaging;
using Foundation;
using System;
using UIKit;
using UserNotifications;
using ObjCRuntime;
using System.Linq;
namespace FCM.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public class AppDelegate : UIApplicationDelegate, IUNUserNotificationCenterDelegate, IMessagingDelegate
    {
        // class-level declarations

        public override UIWindow Window
        {
            get;
            set;
        }

        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            Firebase.Analytics.App.Configure();

            // 為APP註冊遠端推播
            if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))   //iOS10之後
            {
                var authOptions =
                    UNAuthorizationOptions.Alert | 
                    UNAuthorizationOptions.Badge | 
                    UNAuthorizationOptions.Sound;
                UNUserNotificationCenter.Current.RequestAuthorization(authOptions, (granted, error) => {
                    Console.WriteLine(granted);
                });

                //  iOS推播(APNS)
                UNUserNotificationCenter.Current.Delegate = this;

                // iOS推播(FCM)
                Messaging.SharedInstance.RemoteMessageDelegate = this;
            }
            else
            {
                // iOS 9之前
                var allNotificationTypes = 
                    UIUserNotificationType.Alert | 
                    UIUserNotificationType.Badge | 
                    UIUserNotificationType.Sound;
                var settings = UIUserNotificationSettings.GetSettingsForTypes(allNotificationTypes, null);
                UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
            }
            UIApplication.SharedApplication.RegisterForRemoteNotifications();

            return true;
        }



        public override void DidEnterBackground(UIApplication application)
        {
            Messaging.SharedInstance.Disconnect();
        }

        public override void WillEnterForeground(UIApplication application)
        {
            Messaging.SharedInstance.Connect(error => {
                UIAlertView alert = new UIAlertView();
                alert.AddButton("OK");
                if (error != null)
                    alert.Message = "連線至FCM錯誤";
                else
                    alert.Message = "連線至FCM成功";

                alert.Show();
            });
        }



        public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo,
            Action<UIBackgroundFetchResult> completionHandler)
        {
            //前景收到推播由iOS9及之前的版本觸發
            //背景收到推播由iOS所有版本觸發
            ShowAlert(userInfo);

        }

        [Export("userNotificationCenter:willPresentNotification:withCompletionHandler:")]
        public void WillPresentNotification(UNUserNotificationCenter center, UNNotification notification, 
            Action<UNNotificationPresentationOptions> completionHandler)
        {
            //前景收到推播由iOS10及之後的版本觸發
            ShowAlert(notification.Request.Content.UserInfo);
        }

        //10.0BUG修正，DidReceiveRemoteNotification未被觸發(10.1已修正)
        [Export("userNotificationCenter:didReceiveNotificationResponse:withCompletionHandler:")]
        public void DidReceiveNotificationResponse(UNUserNotificationCenter center, UNNotificationResponse response, Action completionHandler)
        {
            ShowAlert(response.Notification.Request.Content.UserInfo);
        }



        public void ApplicationReceivedRemoteMessage(RemoteMessage remoteMessage)
        {
            ShowAlert(remoteMessage.AppData);
        }

        public void ShowAlert(NSDictionary info)
        {
            var apsDictionary = info["aps"] as NSDictionary;
            var data = apsDictionary["alert"] as NSDictionary;
            var title = data["title"].ToString();
            var message = data["body"].ToString();
            new UIAlertView(title, message, null, "OK").Show();
        }

    }
}